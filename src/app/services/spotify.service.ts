import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) { }

  getQuery( query: string){
    const url = `https://api.spotify.com/v1/${ query }`;
    
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBQCkYn1sAjOV8H_UR9EmQ4sf0PNKyk37SDKfJCflI7x6zy9wXS2rtm_ei2ih69b1pKjOWMB-M7ksIduPo'
    });

    return this.http.get(url, { headers });

  }

  getNewReleases(){ 
    return this.getQuery('browse/new-releases')
      .pipe( map( data => data['albums'].items));
  }

  getArtistas(termino){
    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
      .pipe( map( data => data['artists'].items));
  }

  getArtista(id: string){
    return this.getQuery(`artists/${ id }`);
      // .pipe( map( data => data['artists'].items));
  }

  getTopTracks(id: string){
    return this.getQuery(`artists/${id}/top-tracks?country=us`)
      .pipe( map( data => data['tracks']));
  }



  

}
